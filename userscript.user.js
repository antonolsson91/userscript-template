// ==UserScript==

// @version     1.0.0
// @name        Userscript Name
// @description Describe purpose of userscript
// @namespace   Unique identifier for your script, perhaps an e-mail contact adress or homepage URL
// @author

// @resource    main css/main.css

// Libraries:   https://developers.google.com/speed/libraries/
// @require     //ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js
// @require     js/functions.js
// @require     js/main.js

// @match

// @grant       GM_getResourceText

// @downloadURL https://bitbucket.org/antonolsson91/userscript-template/raw/master/userscript.user.js
// @updateURL   https://bitbucket.org/antonolsson91/userscript-template/raw/master/userscript.user.js

// ==/UserScript==
